#!/usr/bin/env bash
##
# Seed Users
#
# Creates dummy users for testing.
#
# Run from root of the code repository.
#
# This script is not automatically triggered by Grunt, and must be run/automated
# separately if desired in a given environment.
##

drush @generated user-create "generatedadmin" --password="admin1" --mail="generatedadmin@example.com"
drush @generated user-add-role "administrator" "generatedadmin"
