<?php
/**
 * Drush Aliases
 *
 * This file provides for the automatic generation of site aliases based
 * on the file layout and configuration of the Docker hosting environment.
 *
 * Site alias for tuned Drush usage with the 'generated' site.
 */

$host = getenv('APP_DOMAIN');

$aliases['generated'] = array(
  'uri' => $host ? $host : 'http://www.didyoumean.vm/',
  'root' => '/var/www/build/html',
);
